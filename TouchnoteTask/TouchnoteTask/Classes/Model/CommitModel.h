//
//  CommitModel.h
//  TouchnoteTask
//
//  Created by Manuel de la Mata Sáez on 17/01/14.
//  Copyright (c) 2014 mms. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AuthorModel.h"

@interface CommitModel : NSObject

@property (nonatomic, strong) NSString *sha;
@property (nonatomic, strong) AuthorModel *author;
@property (nonatomic, strong) NSString *message;

- (id) initWithDictionary: (NSDictionary *) dictionary;

@end
