//
//  AuthorModel.h
//  TouchnoteTask
//
//  Created by Manuel de la Mata Sáez on 17/01/14.
//  Copyright (c) 2014 mms. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AuthorModel : NSObject

@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *realname;
@property (nonatomic, strong) NSString *avatarUrl;

- (id) initWithDictionary: (NSDictionary *) dictionary;

@end
