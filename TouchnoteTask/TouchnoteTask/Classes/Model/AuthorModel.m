//
//  AuthorModel.m
//  TouchnoteTask
//
//  Created by Manuel de la Mata Sáez on 17/01/14.
//  Copyright (c) 2014 mms. All rights reserved.
//

#import "AuthorModel.h"

@implementation AuthorModel

- (id) initWithDictionary: (NSDictionary *) dictionary{
    
    self = [super init];
    if (self) {
        [self hydrateWithDictionary: dictionary];
    }
    return self;
}

- (void) hydrateWithDictionary: (NSDictionary *) dictionary
{
    self.username = dictionary[@"login"]?:@"";
    self.avatarUrl = dictionary[@"avatar_url"]?:@"";
}


@end
