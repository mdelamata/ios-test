//
//  CommitsViewController.m
//  TouchnoteTask
//
//  Created by Manuel de la Mata Sáez on 17/01/14.
//  Copyright (c) 2014 mms. All rights reserved.
//

#import "CommitsViewController.h"
#import "CommitsDAO.h"
#import "CommitCellView.h"
#import "CommitModel.h"

@interface CommitsViewController ()

-(void)configureView;
-(void)refreshData;
-(void)prepareData:(NSArray*)commits;
-(void)notifyError;

@property (nonatomic, retain) NSMutableArray *usernamesArray;               //used for the sections
@property (nonatomic, retain) NSMutableDictionary *dataSourceDictionary;    //the dataSource of the mainTable

@end

@implementation CommitsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title =@"Touchnote.com Task";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self configureView];
    [self refreshData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Privated Methods

//method that configures the view
-(void)configureView{
    
    //adds a button for refreshing
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshData)];
    self.navigationItem.rightBarButtonItem = addButton;
    
    UIBarButtonItem *infoButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(aboutInfo:)];
    self.navigationItem.leftBarButtonItem = infoButton;

    
    //set the delegates of mainTable
    [self.commitsTableView setDelegate:self];
    [self.commitsTableView setDataSource:self];
    [self.commitsTableView setAlpha:0];

}

//method that will request the commits to the service
-(void)refreshData{
    
    //checks if it can request or not
    if ([TouchNotesUtils checkInternetConnection]) {
        
        //simple fade animation
        [UIView animateWithDuration:1 animations:^{
            [self.commitsTableView setAlpha:0];
        }];
        
        __weak typeof(self) weakSelf = self;
        
        //performs the request
        [[CommitsDAO sharedInstance] requestCommitsWithSuccess:^(NSArray *commits) {
            [weakSelf prepareData:commits];
        } failure:^{
            [weakSelf notifyError];
        }];
        
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"There is no internet connection, sorry..."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [alert show];
        
    }
   
    
}

//method that shows info from the user
-(void)aboutInfo:(id)sender{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hello Touchnote Team!"
                                                    message:@"I hope you like this...\nManuel de la Mata Sáez \n 2014"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    [alert show];
    
}

//method that prepares the data. It's better to reorganize the data as you wish from a clean source.
-(void)prepareData:(NSArray *)commits{
    
    self.usernamesArray = [@[] mutableCopy];
  
    NSMutableDictionary *myDict = [NSMutableDictionary dictionary];
    
    //this loop will reorganize as I wish. Group by name all the commits.
    for (CommitModel *commit in commits){
        
        if([myDict objectForKey: commit.author.username]){
            //gets and adds the commit for existent user
            NSMutableArray *array = [myDict objectForKey: commit.author.username];
            [array addObject:commit];
            [myDict setObject:array forKey:commit.author.username];
        }else{
            //new username
            NSMutableArray *array = [NSMutableArray arrayWithObject:commit];
            [myDict setObject:array forKey:commit.author.username];
            
            //hydrates the sections
            [self.usernamesArray addObject:commit.author.username];
        }
    }
    
    //this will be used
    self.dataSourceDictionary = myDict;
    
    [self.commitsTableView reloadData];
    
    //simple fade animation
    [UIView animateWithDuration:1 animations:^{
        [self.commitsTableView setAlpha:1];
    }];

}


//method that shows an alert to warn the user that the request failed
-(void)notifyError{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                    message:@"Something went wrong... Please, repeat later."
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    [alert show];
}

//helper method to get all the commits from a user
-(NSArray*)getAllCommitsMessageFromUser:(NSString*)username{
    
    NSMutableArray *messagesArray = [@[]mutableCopy];
    NSArray *commitsForUser = [self.dataSourceDictionary objectForKey:username];
    
    for (CommitModel *commit in commitsForUser) {
        if (commit.message) {
            [messagesArray addObject:commit.message];
        }
    }
   return [NSArray arrayWithArray:messagesArray];
}


#pragma mark - Table view data source


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CommitModel *commit = [[self.dataSourceDictionary objectForKey:[self.usernamesArray objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] ;
    
    //calculates if the height of the cell needs to be bigger
    NSInteger numOfCommitsToShow = [self getAllCommitsMessageFromUser:commit.author.username].count;

    CGFloat heightForCell = STANDARD_HEIGHT_OF_CELL;
    if (numOfCommitsToShow> 2) { // it there is more than 2 commits it needs to grow... this should have been done better, sorry.
        heightForCell = STANDARD_HEIGHT_OF_CELL + STANDARD_HEIGHT_OF_COMMIT_LABEL * (numOfCommitsToShow-2);
    }
    
    return heightForCell;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CommitCellView *cell = nil;
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CommitCellView" owner:nil options:nil];
    
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[CommitCellView class]])
        {
            cell = (CommitCellView *)currentObject;
            [cell configureRow];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            break;
        }
    }

    
    // customizes the cell with commit data
    CommitModel *commit = [[self.dataSourceDictionary objectForKey:[self.usernamesArray objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] ;
    
    [[cell nameLabel] setText: commit.author.realname];
    [cell hydrateWithCommits:[self getAllCommitsMessageFromUser:commit.author.username]];
    
    //request to download the imageURL asynchronously
    [TouchNotesUtils downloadImageWithURL:[NSURL URLWithString:commit.author.avatarUrl] completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            [[cell avatarImageView] setImage:image];
        }
    }];
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    // Return the height of sections.
    return 20;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    // Return the number of sections.
//    return [self.self.usernamesArray count];
    return [self.usernamesArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    return 1;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    //name of section
    return self.usernamesArray[section];
}

@end
