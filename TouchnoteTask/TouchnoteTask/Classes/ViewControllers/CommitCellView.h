//
//  CommitCellView.h
//  TouchnoteTask
//
//  Created by Manuel de la Mata Sáez on 17/01/14.
//  Copyright (c) 2014 mms. All rights reserved.
//

#import <UIKit/UIKit.h>

#define STANDARD_HEIGHT_OF_CELL 90
#define STANDARD_HEIGHT_OF_COMMIT_LABEL 20

@interface CommitCellView : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UIView *commitsView;

+(NSString*)reuseIdentifier;
-(void)configureRow;
-(void)hydrateWithCommits:(NSArray*)commits;

@end
